/*
**  Sound grabber for jm on the PC
**  v1.0�1
**  by Jason Blochowiak
*/

#include <GSOS.h>
#include <Locator.h>
#include <Memory.h>
#include <MiscTool.h>
#include <TextTool.h>

#define Compression true
#define MaxSounds   128

typedef struct
        {
            longword    offset,
                        length,
                        hertz;
            byte        bits,
                        reference;
        } SS, *SSp;
    
    char            *names[] =
                    {
#if 0
                        "9:Dead.Bang",
                        "9:Snd.UhOh",
#else
                        "9:Fire.Snd",
                        "9:BumpWall.Snd",
                        "9:AfterBurner.Snd",
                        "9:ShootWall.Snd",
                        "9:ShootThing.Snd",
                        "9:SaveHostage.Snd",
                        "9:HostageDead.Snd",
                        "9:LowTime.Snd",
                        "9:TakeDamage.Snd",
                        "9:Nuke.Snd",
                        "9:WarpGate.Snd",
                        "9:PlayerDead.Snd",
                        "9:GunReady.Snd",
                        "9:MaxPower.Snd",
                        "9:LastHostage.Snd",
                        "9:ArmorUp.Snd",
                        "9:TimeScore.Snd",
                        "9:GuyScore.Snd",
                        "9:StartGame.Snd",
                        "9:HighScore.Snd",
#endif
                        ""
                    };
    word            MyID,SoundID,
                    SoundCnt;
    
    Handle          sh[MaxSounds];
    SS              sounds[MaxSounds];
    
    CreateRecGS     crRec;
    OpenRecGS       oRec;
    IORecGS         ioRec;
    GSString255     temp;

void
CToTemp(s)
    char    *s;
{
    temp.length = strlen(s);
    BlockMove(s,temp.text,(longword)temp.length);
}

/*
**  Creates a file of the specified type & auxtype
*/
boolean
CreateFile(path,type,aux)
    char        *path;
    word        type;
    longword    aux;
{
    boolean     tried;
    
    tried = false;
    
    CToTemp(path);
    
    crRec.pCount = 4;
    crRec.pathname = &temp;
    crRec.access = 0xc3;
    crRec.fileType = type;
    crRec.auxType = aux;
    
do_create:
    CreateGS(&crRec);
    if ((_toolErr == dupPathname) && !tried)
    {
        tried = true;
        
        crRec.pCount = 1;
        DestroyGS(&crRec);
        crRec.pCount = 4;
        
        if (!_toolErr)
            goto do_create;
    }
    return(!_toolErr);
}

/*
**
*/
word
OpenFile(path)
    char    *path;
{
    CToTemp(path);
    
    oRec.pCount = 2;
    oRec.pathname = &temp;
    OpenGS(&oRec);
    if (_toolErr)
        return(0);
    else
        return(oRec.refNum);
}

/*
**
*/
void
CloseFile(num)
    word    num;
{
    oRec.pCount = 1;
    oRec.refNum = num;
    CloseGS(&oRec);
}

/*
**  Writes a chunk of memory to the specified file
*/
boolean
WriteChunk(refnum,data,len)
    word    refnum;
    Ptr     data;
    word    len;
{
    ioRec.pCount = 4;
    ioRec.refNum = refnum;
    ioRec.dataBuffer = data;
    ioRec.requestCount = len;
    
    WriteGS(&ioRec);
    if (_toolErr || (ioRec.transferCount != ioRec.requestCount))
        return(false);
    return(true);
}

boolean
WriteHChunk(refnum,data,len)
    word    refnum;
    Handle  data;
    word    len;
{
    boolean result;
    
    HLock(data);
    result = WriteChunk(refnum,*data,len);
    HUnlock(data);
    return(result);
}

/*
**  Reads an entire file in as an image
*/
Handle
ReadImage(path,id)
    char    *path;
    word    id;
{
    Handle  result;
    
    CToTemp(path);
    
    oRec.pCount = 12;
    oRec.pathname = &temp;
    oRec.access = readEnable;
    oRec.resourceNumber = 0;
    oRec.optionList = nil;
    OpenGS(&oRec);
    if (_toolErr)
        return(nil);
    result = NewHandle(oRec.eof,id,attrLocked,nil);
    if (_toolErr)
        goto error_close;
    ioRec.pCount = 4;
    ioRec.refNum = oRec.refNum;
    ioRec.dataBuffer = *result;
    ioRec.requestCount = oRec.eof;
    ReadGS(&ioRec);
    if (_toolErr || (ioRec.transferCount < ioRec.requestCount))
    {
        DisposeHandle(result);
error_close:
        result = nil;
    }
    oRec.pCount = 1;
    CloseGS(&oRec);
    if (result)
        HUnlock(result);
    
    return(result);
}

void
DoSound(path)
    char        *path;
{
    byte        last,cur;
    int         delta,slop;
    longword    i,offset,rate,size;
    Ptr         s,cm;
    SSp         snd;
    Handle      sample,compressed;
    
    sample = ReadImage(path,SoundID);
    
    snd = sounds + SoundCnt;
    
    s = *sample;
    offset = 8 + *((longword *)(s + 4));
    
    rate = *((word *)(s + 28));
    snd->hertz = (rate * 1645) / 32;
    
    size = GetHandleSize(sample) - offset;
    BlockMove(s + offset,s,size);   /* Move data "up" over header info */
    SetHandleSize(size,sample);     /* Shrink handle */
    sh[SoundCnt] = sample;
    
#if Compression
    snd->bits = 4;
    snd->reference = last = **sample;
    
    BlockMove((*sample) + 1,*sample,--size);
    if (size & 1)   /* Make sure we have an even number of nibbles */
    {
        (*sample)[size] = (*sample)[size - 1];
        size++;
    }
    else
        SetHandleSize(size,sample);
    
    compressed = NewHandle(size / 2,SoundID,0,nil);
    for (i = 0,s = *sample,cm = *compressed,slop = 0;i < size / 2;i++,cm++)
    {
        cur = *s++;
        delta = cur - last;
        if (delta < -8)
            delta = -8;
        else if (delta > 7)
            delta = 7;
        last += delta;
        *cm = delta & 0x0f;
        
        cur = *s++;
        delta = cur - last;
        if (delta < -8)
            delta = -8;
        else if (delta > 7)
            delta = 7;
        last += delta;
        *cm |= (delta & 0x0f) << 4;
    }
    
    DisposeHandle(sample);
    sample = compressed;
    sh[SoundCnt] = sample;
    snd->length = size / 2;
    printf("[%ld]",size);
#else
    snd->bits = 8;
    snd->reference = 0;
    snd->length = size;
#endif
    
    printf("%s -> rate=%ldHz length=%ld bytes\n",path,snd->hertz,snd->length);
    SoundCnt++;
}

void
DoIt()
{
    word            i,
                    refnum;
    longword        offset;
    Handle          h;
    
    oRec.pCount = 0;
    BeginSession(&oRec);
    
    CreateFile("9:Test.SSD",0x06,0x0000L);
    refnum = OpenFile("9:TEST.SSD");
    
    SoundCnt = 0;
    for (i = 0;*(names[i]);i++)
        DoSound(names[i]);
    
    offset = sizeof(SS) * SoundCnt;
    for (i = 0;i < SoundCnt;i++)
    {
        sounds[i].offset = offset;
        offset += GetHandleSize(sh[i]);
    }
    
    for (i = 0;i < SoundCnt;i++)
        WriteChunk(refnum,sounds + i,sizeof(SS));
    
    for (i = 0;i < SoundCnt;i++)
        WriteHChunk(refnum,sh[i],(word)GetHandleSize(sh[i]));
    
    CloseFile(refnum);
    
    oRec.pCount = 0;
    EndSession(&oRec);
}

main()
{
    WriteCString("jm Sound Grabber v1.0d1\r\n");
    
    MyID = MMStartUp();
    SoundID = MyID | 0x100;
    
    DoIt();
    ReadChar(false);
    
    DisposeAll(SoundID);
    
    MMShutDown(MyID);
}
