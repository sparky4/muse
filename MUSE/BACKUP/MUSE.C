/////////////////////////////////////////////////
//
// MUSE - Music Utility and Sound Editor
// by John Romero (C) 1991 Id Software
//
// Project Journal
// ----------------------------------------------
// May 29 91 - Getting all the files together that
// 	I need to get this pup rolling. Defining
//	all the functions and structures, etc.
//
// Jun  4 91 - Got the DELTADRAW stuff working
//	just great. Starting to define the info
//	file structure, etc.
//
// Jun  5 91 - Got the Sound Manager integrated.
//	Getting stuff set up for playing...
//
// Jun 10 91 - Just got back from Kansas D&D Fest.
//	Minor debugging & trying to get AdLib sound
//	playing...
//
// Jun 14 91 - Getting the saving done. Jason found
//	out that Stack Overflow Checking in interrupt
//	routines will toast the system!
//
// Jun 16 91 - Restructured stuff...got saving 1/2
//	done...adding more features...
//
// Jun 19 91 - Got AdLib playing/instrument change;
//	.BNK file change; lots of little shit...
//
// Jun 27 91 - Lots of TED5 and IGRAB fixes and
//	function additions...Working on getting MUSIC
//	integrated & converted...
//
// Jun 30 91 - Got priorities in...Jason figured out
//	why the instruments were toasty...fixed a
//	couple small bugs...trying to get MUSIC in...
//
// Jul  8 91 - Well, the Id Seminar is over and we're
//	all ready to go back to work! Found a couple
//	bugs: FIXED. Adding more stuff...getting ready
//	to Import the digitized stuff...
// Jul 24 91 - Only writes the .H when it needs to!
//
// Aug 16 91 - Got MUSIC importing just about done,
//	there's STILL a problem playing back music.
//	Of course, LOTS of stuff got stuck in MUSE,
//	I just didn't tell ya about it!
//
// Aug 18 91 - Finished all MUSIC functions. The
//	ROL->ID conversion may have a bug or two left,
//	but it's clean overall. Update Imports is done.
//
// Aug 19 91 - Adding support for percussive music.
//
// Oct 1  91 - Beeps on selecting percussive instrument,
//	puts Staged-Write warning at total start.
//
// Oct 3  91 - Let user select DATA or FARDATA generation
//	for Huffman dictionary .OBJ.
//
/////////////////////////////////////////////////
#include "muse.h"
#pragma hdrstop

//
// Variables
//
MUSEInfoStr MUSEinfo;
PCStr PCsound[MAXSOUNDS];
AdLibStr AdLibsnd[MAXSOUNDS];
DigiStr Digisound[MAXSOUNDS];
datatype whichtype;
MInfoStr musicinfo[MAXMUSIC];

char MUSEversion[]="        MUSE v0.18\n";
char pnames[MAXPROJS][14];
byte adtranstable[256],adtrans2table[256];

char soundnames[MAXSOUNDS][16],projname[14]="AUDIOT.",infoname[14]="MUSEINFO.",
	ext[4],digifnames[MAXSOUNDS][16],bankname[64];
char _seg *InstBank,UNDObuf[DATAPOINTS];
int pixelx,pixely,lastx,lasty,DirtyFlag,LockY,writeH;
word selectx1,selectx2,selectstate,whichsound,PClookup[WINDOW_BOTTOM];
byte b0,b1,deltamode=1;
Instrument definst = {0x21,0x11,0x11,0x00,0xa3,0xc4,0x43,0x22,0x02,0x00,0x0d,
			0x00,0x00,0x00,0x00,0x00};

//
// harderr-called routine
//
int resume(void)
{
 hardresume(0);
 return 0;
}

/////////////////////////////////////////////////
//
// Start of the whole shmear!
//
/////////////////////////////////////////////////
void main(void)
{
 SetupKBD();
 MMStartup();
 setvideo(EGA1);
 InitDesktop(&MUSE_MBar[0],1);
 SD_Startup();
 MouseShow();
 if (Message("WARNING: You must de-activate ALL\n"
	   "disk-caches that are set for\n"
	   "STAGED WRITES (i.e., Hyperdisk).\n"
	   "Continue?")==1)
   Quit("");
 InitMUSE();
 harderr(resume);
 DeskEventLoop(HandleEvent,Continuous);
}


/////////////////////////////////////////////////
//
// The mouse button was pressed!
//
/////////////////////////////////////////////////
void HandleEvent(void)
{
 MouseCoords(&(int)pixelx,&(int)pixely);
 b0=MouseButton()&1;
 b1=MouseButton()&2;

 CheckMUSEbuttons();

}

/////////////////////////////////////////////////
//
// Called continuously, hence the name!
//
/////////////////////////////////////////////////
void Continuous(void)
{
 byte huge *Data;
 int i,j;


 MouseCoords(&(int)pixelx,&(int)pixely);
 b0=MouseButton()&1;
 b1=MouseButton()&2;

 //
 // THIS STUFF HANDLES DRAWING IN THE EDIT WINDOW
 //
 if (pixelx>=WINDOW_LEFT && pixelx<WINDOW_RIGHT &&
     pixely>=WINDOW_TOP && pixely<=WINDOW_BOTTOM)
   {
    //
    // LOCK THE Y-AXIS
    //
    if (keydown[0x1d])
      {
       if (!LockY)
	 LockY=pixely;
       pixely=LockY;
      }
    else
      LockY=0;

    //
    // FIND END OF CURRENT SOUND
    //
    if (!b0)
      {
       if (lastx)	// DID WE ALREADY SEARCH?
	 {
	  FindDataLength();
	  DrawEndLine();
	  lastx=0;
	 }
      }

    //
    // STORE NEW SOUND DATA?
    //
    if (b0)
      {
       //
       // Make sure we assign "Data"
       //
       Data=GetDataAddr();

       MouseHide();
       if (selectstate)
	 {
	  for (i=selectx1;i<=selectx2;i++)
	    if (Data[i-WINDOW_LEFT]>=WINDOW_TOP)
	      {
	       VW_Vlin(WINDOW_TOP,Data[i-WINDOW_LEFT],i,EDBKGND);
	       VW_Vlin(Data[i-WINDOW_LEFT],WINDOW_BOTTOM,i,EDCOLOR);
	      }
	    else
	      VW_Vlin(WINDOW_TOP,WINDOW_BOTTOM,i,EDBKGND);

	  selectx1=selectx2=selectstate=0;
	 }

       //
       // HANDLE DELTADRAW MODE
       //
       if (deltamode)
	 {
	  if (!lastx)
	    {
	     lastx=pixelx;
	     lasty=pixely;

	     if (pixelx-WINDOW_LEFT>GetDataLen())
	       {
		MouseHide();
		VW_Vlin(WINDOW_TOP,WINDOW_BOTTOM,GetDataLen()+WINDOW_LEFT,EDBKGND);
		MouseShow();
	       }
	    }
	  else
	    {
	     word x1,x2,y1,y2,temp;
	     float slopey,temp1,temp2;

	     if (pixelx<lastx)
	       {
		slopey=(float)(pixely-lasty)/(lastx-pixelx);

		for (j=0,i=lastx;i>pixelx;i--,j++)
		  {
		   temp1=slopey*j+lasty;
		   if (temp1<WINDOW_TOP)
		     temp1=WINDOW_TOP;
		   if (temp1>WINDOW_BOTTOM)
		     temp1=WINDOW_BOTTOM;

		   Data[i-WINDOW_LEFT]=temp1;
		   DirtyFlag=1;
		   VW_Vlin(WINDOW_TOP,temp1,i,EDBKGND);
		   VW_Vlin(temp1,WINDOW_BOTTOM,i,EDCOLOR);
		  }
	       }
	     else
	     if (pixelx>lastx)
	       {
		slopey=(float)(pixely-lasty)/(pixelx-lastx);

		for (i=lastx,j=0;i<pixelx;j++,i++)
		  {
		   temp1=slopey*j+lasty;
		   Data[i-WINDOW_LEFT]=temp1;
		   DirtyFlag=1;
		   VW_Vlin(WINDOW_TOP,temp1,i,EDBKGND);
		   VW_Vlin(temp1,WINDOW_BOTTOM,i,EDCOLOR);
		  }
	       }
	     else
	       {
		Data[pixelx-WINDOW_LEFT]=pixely;
		DirtyFlag=1;
		VW_Vlin(WINDOW_TOP,pixely,pixelx,EDBKGND);
		VW_Vlin(pixely,WINDOW_BOTTOM,pixelx,EDCOLOR);
	       }

	     lastx=pixelx;
	     lasty=pixely;
	    }
	 }
       else
	 {
	  if (pixelx-WINDOW_LEFT>GetDataLen())
	    {
	     MouseHide();
	     VW_Vlin(WINDOW_TOP,WINDOW_BOTTOM,GetDataLen()+WINDOW_LEFT,EDBKGND);
	     MouseShow();
	    }

	  lastx=1;

	  VW_Vlin(WINDOW_TOP,pixely,pixelx,EDBKGND);
	  VW_Vlin(pixely,WINDOW_BOTTOM,pixelx,EDCOLOR);
	  Data[pixelx-WINDOW_LEFT]=pixely;
	  DirtyFlag=1;
	 }
       MouseShow();
      }
    //
    // HANDLE HIGHLIGHT REGION
    //
    else
     {
      //
      // Make sure we assign "Data"
      //
      Data=GetDataAddr();

      switch(selectstate)
      {
       case 0: // INIT
	 if (b1)
	   {
	    if (pixelx>=GetDataLen()+WINDOW_LEFT)
	      break;

	    selectstate++;
	    selectx2=selectx1=pixelx;
	    MouseHide();
	    if (Data[pixelx-WINDOW_LEFT]>=WINDOW_TOP)
	      {
	       VW_Vlin(WINDOW_TOP,Data[pixelx-WINDOW_LEFT],pixelx,EDHIGHB);
	       VW_Vlin(Data[pixelx-WINDOW_LEFT],WINDOW_BOTTOM,pixelx,EDHIGHC);
	      }
	    else
	      VW_Vlin(WINDOW_TOP,WINDOW_BOTTOM,pixelx,EDHIGHB);
	    MouseShow();
	   }
	 break;
       case 1: // DRAGGING || OFF
	 if (b1)
	   {
	    if (pixelx>=GetDataLen()+WINDOW_LEFT)
	      break;


	    if (pixelx<selectx1)
	      selectx1=pixelx;
	    else
	    if (pixelx>selectx2)
	      selectx2=pixelx;

	    MouseHide();
	    for (i=selectx1;i<=selectx2;i++)
	      if (Data[i-WINDOW_LEFT]>=WINDOW_TOP)
		{
		 VW_Vlin(WINDOW_TOP,Data[i-WINDOW_LEFT],i,EDHIGHB);
		 VW_Vlin(Data[i-WINDOW_LEFT],WINDOW_BOTTOM,i,EDHIGHC);
		}
	      else
		VW_Vlin(WINDOW_TOP,WINDOW_BOTTOM,i,EDHIGHB);
	    MouseShow();
	   }
	 else
	   {
	    selectstate=2;
	   }
	 break;
       case 2: // TURN OFF SELECT
	 if (b1)
	   {
	    MouseHide();
	    for (i=selectx1;i<=selectx2;i++)
	       if (Data[i-WINDOW_LEFT]>=WINDOW_TOP)
		 {
		  VW_Vlin(WINDOW_TOP,Data[i-WINDOW_LEFT],i,EDBKGND);
		  VW_Vlin(Data[i-WINDOW_LEFT],WINDOW_BOTTOM,i,EDCOLOR);
		 }
	       else
		 VW_Vlin(WINDOW_TOP,WINDOW_BOTTOM,i,EDBKGND);
	    MouseShow();
	    selectx1=selectx2=selectstate=0;
	   }
      }
     }
   }
}


/////////////////////////////////////////////////
//
// Init MUSE
//
/////////////////////////////////////////////////
void InitMUSE(void)
{
 word i,temp;


 if (!MouseStatus)
   {
    ErrDialog("No mouse, no MUSE!"," Press ENTER ");
    Quit("Get a mouse, dammit!");
   }

 ErrDialog("Initializing. One moment.","");

 //
 // INIT ADLIB TRANSLATE TABLE
 //
 for (i=0;i<256;i++)
   {
    adtranstable[i]=(255l-i)*SCREENPOINTS/256+WINDOW_TOP;
    adtrans2table[adtranstable[i]]=i;
   }

 InitProject();
 InitBNKfile();
 //
 // DONE INITING
 //
 RestoreBackground();
 MouseOrigin(320,125);
 DrawMUSEscreen(1);
 Item_About();
}


void PatchPointers(void)
{
}

void Unhook(void)
{
 SD_Shutdown();
 ShutdownKBD();
 MMShutdown();
}


/////////////////////////////////////////////////
//
// Init this hairy nastiness
//
/////////////////////////////////////////////////
void InitProject(void)
{
 struct ffblk f;
 char sname[14];
 word i,dx,dy;

 //
 // See if a project exists
 //
 strcpy(sname,projname);
 strcat(sname,"*");

 if (!findfirst(sname,&f,FA_ARCH))
   //
   // FOUND AN EXISTING PROJECT!
   //
   LoadNewProject();
 else
   //
   // INITIALIZE A TOTALLY NEW PROJECT!
   //
   InitNewProject();

 SaveUNDO();
}

void ProjOn(int x,int y,int which)
{
 xormask=1;
 ProjOff(x,y,which);
 xormask=0;
}

void ProjOff(int x,int y,int which)
{
 sx=x;
 sy=y;
 MouseHide();
 print(pnames[which]);
 MouseShow();
}


/////////////////////////////////////////////////
//
// Init a .BNK file (Instrument Bank)
//
/////////////////////////////////////////////////
void InitBNKfile(void)
{
 char temp[64],path[64];
 struct ffblk f;
 int i;


 //
 // LOOK IN THE "MUSE" DIRECTORY FOR A .BNK FILE!
 //
 strcpy(path,_argv[0]);
 for (i=strlen(path);i>=0;i--)
   if (path[i]=='\\')
     {
      path[i+1]=0;
      break;
     }
 strcpy(temp,path);
 strcat(temp,"*.BNK");


 if (!findfirst(temp,&f,FA_ARCH))
   {
    strcpy(bankname,path);
    strcat(bankname,f.ff_name);
    LoadIn(bankname,(memptr *)&InstBank);
   }
 else
   {
    ErrDialog("I can't find an AdLib\n"
	      "Instrument Bank file (.BNK)\n"
	      "in the MUSE directory!\n"
	      "You won't be able to change\n"
	      "your instrument!\n"," OK ");
    return;
   }
}


/////////////////////////////////////////////////
//
// Init a new project from EXT
//
/////////////////////////////////////////////////
btype INPRJb={"    ",8,3,1};
DialogDef PROJd={"Which SoundProject do\n"
		 "you want to deal with?",22,14,0,0,0},
	  INPRJd={"Input your project's\n"
		  "file extension",20,5,1,&INPRJb,NULL};

void InitNewProject(void)
{
 int i;


 //
 // SET UP FIRST SOUND
 //
 MMAllocate((memptr *)&PCsound[0].data,PCBUFSIZE);
 MMAllocate((memptr *)&AdLibsnd[0].data,ALBUFSIZE);
 for (i=0;i<DATAPOINTS;i++)
   PCsound[0].data->data[i]=AdLibsnd[0].data->data[i]=0;
 PCsound[0].data->common.length=PCsound[0].data->common.priority=0;

 AdLibsnd[0].data->inst = definst;
 strcpy(AdLibsnd[0].instname,"DEFAULT");
 AdLibsnd[0].data->common.length=AdLibsnd[0].data->common.priority=0;
 AdLibsnd[0].data->block=4;

 Digisound[0].type=0;

 //
 // GET PROJECT EXTENSION
 //
 DrawDialog(&INPRJd,1);
 GetButtonXY(&INPRJd,0,&sx,&sy);
 MouseHide();
 if (!input(ext,3))
   {
    errout("Aborted by user (that's YOU!)");
   }
 RestoreBackground();
 MouseShow();

 strupr(ext);
 strcpy(projname,"AUDIOT.");
 strcat(projname,ext);
 strcpy(infoname,"MUSEINFO.");
 strcat(infoname,ext);

 strcpy(soundnames[0],"Untitled0");
 memset(&MUSEinfo,0,sizeof(MUSEinfo));
 MUSEinfo.num_sounds=1;
 writeH=1;
 SaveAudio();
}


/////////////////////////////////////////////////
//
// Select a new project file
// NOTE: must already know that there are projects available
//
/////////////////////////////////////////////////
void LoadNewProject(void)
{
 struct ffblk f;
 char sname[14];
 int num=1,select=-1,i;
 word dx,dy;


 strcpy(sname,"AUDIOT.*");
 findfirst(sname,&f,FA_ARCH);
 //
 // Build project list
 //
 strcpy(pnames[0],f.ff_name);
 while((!findnext(&f)) && num<MAXPROJS)
   strcpy(pnames[num++],f.ff_name);

 if (num>1)
   {
    //
    // Draw project dialog
    //
    MouseHide();
    DrawDialog(&PROJd,1);
    GetDialogXY(&PROJd,&sx,&sy);
    dx=sx;
    dy=sy;
    DrawBorder(sx+5,sy+2,11,11,1);
    for (i=0;i<num;i++)
      {
       sx=dx+6;
       sy=dy+3+i;
       print(pnames[i]);
      }
    MouseShow();

    while(select<0)
      select=CheckList(dx+6,dy+3,11,num,ProjOn,ProjOff,1);
   }
 else
   select=0;

 strcpy(ext,pnames[select]+7);
 strcpy(projname,"AUDIOT.");
 strcat(projname,ext);
 strcpy(infoname,"MUSEINFO.");
 strcat(infoname,ext);

 LoadAudio();
}


/////////////////////////////////////////////////
//
// errout routine
//
/////////////////////////////////////////////////
void errout(char *string)
{
 Unhook();
 setvideo(TEXT);
 printf("MUSE Error: %s",string);
 exit(1);
}


/////////////////////////////////////////////////
//
// Clear all data in memory - ready new project
//
/////////////////////////////////////////////////
void ClearMemory(void)
{
 int i;

 //
 // TOAST ALL THE STUFF IN MEMORY
 //
 for (i=0;i<MUSEinfo.num_sounds;i++)
   {
    MMFreePtr((memptr *)&PCsound[i].data);
    MMFreePtr((memptr *)&AdLibsnd[i].data);
    if (Digisound[i].type<0)
      MMFreePtr((memptr *)&Digisound[i].data);
   }

 whichsound=selectstate=0;
 selectx1=selectx1=lastx=lasty=-1;
}


#define GC_MODE		5
#define GC_INDEX	0x3ce
#define GC_BITMASK	8
#define SC_INDEX	0x3c4
#define SC_MAPMASK	2


#define EGAWRITEMODE(x) asm{cli;mov dx,GC_INDEX;mov ax,GC_MODE+256*x;out dx,ax;sti;}
#define EGABITMASK(x) asm{cli;mov dx,GC_INDEX;mov ax,GC_BITMASK+256*x;out dx,ax;sti;}
#define EGAMAPMASK(x) asm{cli;mov dx,SC_INDEX;mov ax,SC_MAPMASK+x*256;out dx,ax;sti;}
unsigned char leftmask[8]={0xff,0x7f,0x3f,0x1f,0xf,0x7,3,1},
	      rightmask[8]={0x80,0xc0,0xe0,0xf0,0xf8,0xfc,0xfe,0xff};

/////////////////////////////////////////////////
//
// Bar routine
//
/////////////////////////////////////////////////
void VW_Bar (unsigned x, unsigned y, unsigned width, unsigned height,
	unsigned color)
{
	unsigned screenseg=0xa000,linewidth=40;
	unsigned dest,xh,xlb,xhb,maskleft,maskright,mid;

	xh = x+width-1;
	xlb=x/8;
	xhb=xh/8;

	EGAWRITEMODE(2);
	EGAMAPMASK(15);

	maskleft = leftmask[x&7];
	maskright = rightmask[xh&7];

	mid = xhb-xlb-1;
	dest = y*40+xlb;

	if (xlb==xhb)
	{
	//
	// entire line is in one byte
	//

		maskleft&=maskright;

	asm	mov	es,[screenseg]
	asm	mov	di,[dest]

	asm	mov	dx,GC_INDEX
	asm	mov	al,GC_BITMASK
	asm	mov	ah,[BYTE PTR maskleft]
	asm	out	dx,ax		// mask off pixels

	asm	mov	ah,[BYTE PTR color]
	asm	mov	dx,[linewidth]
yloop1:
	asm	mov	al,ah
	asm	xchg	al,[es:di]	// load latches and write pixels
	asm	add	di,dx			// down to next line
	asm	dec	[height]
	asm	jnz	yloop1

		goto	done;
	}

asm	mov	es,[screenseg]
asm	mov	di,[dest]
asm	mov	bh,[BYTE PTR color]
asm	mov	dx,GC_INDEX
asm	mov	si,[linewidth]
asm	sub	si,[mid]			// add to di at end of line to get to next scan
asm	dec	si

//
// draw left side
//
yloop2:
asm	mov	al,GC_BITMASK
asm	mov	ah,[BYTE PTR maskleft]
asm	out	dx,ax		// mask off pixels

asm	mov	al,bh
asm	mov	bl,[es:di]	// load latches
asm	stosb

//
// draw middle
//
asm	mov	ax,GC_BITMASK + 255*256
asm	out	dx,ax		// no masking

asm	mov	al,bh
asm	mov	cx,[mid]
asm	rep	stosb

//
// draw right side
//
asm	mov	al,GC_BITMASK
asm	mov	ah,[BYTE PTR maskright]
asm	out	dx,ax		// mask off pixels

asm	mov	al,bh
asm	xchg	al,[es:di]	// load latches and write pixels

asm	add	di,si		// move to start of next line
asm	dec	[height]
asm	jnz	yloop2

done:
	EGABITMASK(255);
	EGAWRITEMODE(0);
}

/////////////////////////////////////////////////
//
// MUSE MenuBar
//
/////////////////////////////////////////////////
MenuDef AboutMenu[]=
	{
	 {"About...",Item_About,0,0},
	 {"Choose another Project",Item_NewProject,0,0},
	 {"Create a NEW Project",Item_CreateProject,0,0},
	 {"Memory Available",Item_MemStats,0,0x44}
	},

	FileMenu[]=
	{
	 {"Change Instrument Bank",Item_ChangeBNK,0,0},
	 {"Update Imports",Item_UpdateImports,0,0},
	 {"Huffmanize Audio",Item_HuffAudio,0,0},
	 {"----------------------",NULL,0,0},
	 {"Save AUDIO file",Item_SaveAudio,ALT,0x1f},
	 {"MUSIC Functions",Item_MUSIC,0,0x32},
	 {"DIGITIZE Import",Item_DigiImport,ALT,0x20},
	 {"Quit MUSE",Item_Quit,ALT,0x2d}
	},

	EditMenu[]=
	{
	 {"Copy PC->AdLib",Item_PCtoAdLib,0,0},
	 {"Copy AdLib->PC",Item_AdLibtoPC,0,0},
	 {"PC Edit mode",Item_PC_Mode,0,0},
	 {"AdLib Edit mode",Item_AdLibMode,0,0},
	 {"-----------------",NULL,0,0},
	 {"Delete sound/area",Item_DelArea,0,0x53},
	 {"Clear sound/area",Item_ClearArea,0,0x47},
	 {"Edit Sound Name",Item_EditSndName,0,0x31},
	 {"Edit Priorities",Item_Priorities,0,0x19},
	 {"Insert a space",Item_INSERT,0,0x52},
	 {"UNDO last action",Item_RestoreUNDO,0,0x16}
	},

	MiscMenu[]=
	{
	 {"Delta-draw toggle",Item_DeltaTog,0,0x20},
	 {"Add new sound",Item_CreateNewSound,0,0x1e},
	 {"Raise octave value",Item_RaiseBlock,0,0x48},
	 {"Lower octave value",Item_LowerBlock,0,0x50},
	 {"Change Instrument",Item_ChangeInst,0,0x17},
	 {"Goto Last Sound",Item_Last,0,0x4b},
	 {"Goto Next Sound",Item_Next,0,0x4d},
	 {"Sound GOTO",Item_GOTO,0,0x22},
	 {"Play Current Sound",Item_PlayCurrentSound,0,0x1c}
	};


MBarDef MUSE_MBar[]=
	{
	 {4,AboutMenu," * "},
	 {8,FileMenu,"File"},
	 {11,EditMenu,"Edit"},
	 {9,MiscMenu,"Misc"},
	 {0,NULL,""}
	};
