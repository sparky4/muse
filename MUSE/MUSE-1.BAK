/////////////////////////////////////////////////
/////////////////////////////////////////////////
//
// MUSE-1	Subroutines
//
/////////////////////////////////////////////////
/////////////////////////////////////////////////
#include "muse.h"
#pragma hdrstop


/////////////////////////////////////////////////
//
// Draw the MUSE screen
//
/////////////////////////////////////////////////
btype MainCtrls[]={{"Last",2,22,1},
		   {"Next",8,22,1},
		   {"Play",14,22,2},
		   {" PC ",20,22,1},
		   {"AL/SB",26,22,1},
		   {"Instr",33,22,1}};

void DrawMUSEscreen(int drawdialog)
{
 byte huge *Data;
 int i,dx,dy,len;

 MouseHide();

 if (drawdialog)
   {
    bar(0,1,39,24,' ');
    DrawBorder(0,1,39,19,1);

    for (i=0;i<NUMMAINCTRLS;i++)
      {
       dx=sx=MainCtrls[i].xoff;
       dy=sy=MainCtrls[i].yoff;
       print(MainCtrls[i].text);
       sx=dx;
       sy=dy;
       DrawBorder(sx-1,sy-1,strlen(MainCtrls[i].text)+1,2,MainCtrls[i].border);
      }
   }

 //
 // Draw screen w/current data
 //
 switch(whichtype)
 {
  case PC:
    Data=(byte huge *)PCsound[whichsound].data->data;
    len=PCsound[whichsound].data->common.length;
    break;
  case ADLIB:
    Data=(byte huge *)AdLibsnd[whichsound].data->data;
    len=AdLibsnd[whichsound].data->common.length;
 }

 VW_Bar (WINDOW_LEFT, WINDOW_TOP, WINDOW_RIGHT-WINDOW_LEFT, WINDOW_BOTTOM-WINDOW_TOP+1,
	EDBKGND);

 for(i=WINDOW_LEFT;i<len+WINDOW_LEFT;i++)
   if (Data[i-WINDOW_LEFT]>=WINDOW_TOP && i-WINDOW_LEFT<len)
     VW_Vlin(Data[i-WINDOW_LEFT],WINDOW_BOTTOM,i,EDCOLOR);

 DrawEndLine();
 DrawInfoLine();
 MouseShow();
}


/////////////////////////////////////////////////
//
// Draw INFO line at bottom of screen
//
/////////////////////////////////////////////////
void DrawInfoLine(void)
{
 MouseHide();
 bar(0,24,39,24,' ');
 //
 // DELTA mode info
 //
 sy=24;
 sx=0;
 print("Delta ");
 if (deltamode)
   print("On ");
 else
   print("Off");

 //
 // MODE info
 //
 sx=10;
 switch(whichtype)
 {
  case PC:
    print("PC   ");
    break;
  case ADLIB:
    print("AL/SB");
 }

 //
 // SOUND NAME
 //
 sx=16;
 printint(whichsound);
 print("  ");

 sx=19;
 print(soundnames[whichsound]);

 //
 // If AdLib sound, print block
 //
 if (whichtype==ADLIB)
   {
    sx=34;
    print("OCTV:");
    printint(AdLibsnd[whichsound].data->block);
   }

 MouseShow();
}


/////////////////////////////////////////////////
//
// Check MUSE buttons
//
/////////////////////////////////////////////////
void CheckMUSEbuttons(void)
{
 int i,x,y,bx,by,wid,select=0;

 if (!MouseButton())
   return;

 x=pixelx/8;
 y=pixely/8;

 for (i=0;i<NUMMAINCTRLS;i++)
   {
    bx=MainCtrls[i].xoff;
    by=MainCtrls[i].yoff;
    wid=strlen(MainCtrls[i].text);

    if (x>=bx && x<=bx+wid && y==by)
      {
       sx=bx;
       sy=by;
       MouseHide();
       xormask=1;
       print(MainCtrls[i].text);
       xormask=0;
       MouseShow();

       while(MouseButton());

       sx=bx;
       sy=by;
       MouseHide();
       print(MainCtrls[i].text);
       MouseShow();

       select=i+1;
       break;
      }
   }

 switch(select)
 {
  case 1:	// LAST
    if (whichsound)
      {
       whichsound--;
       SaveUNDO();
       selectstate=0;
       DrawMUSEscreen(0);
      }
    break;
  case 2:	// NEXT
    if (PCsound[whichsound+1].data)
      {
       whichsound++;
       SaveUNDO();
       selectstate=0;
       DrawMUSEscreen(0);
      }
    else
      if (Message("Do you want to\nAdd a New Sound?")==2)
	Item_CreateNewSound();

    break;
  case 3:	// PLAY
    Item_PlayCurrentSound();
    break;
  case 4:	// PC
    if (whichtype!=PC)
      Item_PC_Mode();
    break;
  case 5:	// ADLIB
    if (whichtype!=ADLIB)
      Item_AdLibMode();
    break;
  case 6:	// INST
    if (bankname[0])
      Item_ChangeInst();
 }
}


/////////////////////////////////////////////////
//
// Save the AUDIO.ext file out
//
/////////////////////////////////////////////////
void SaveAudio(void)
{
 word i;
 long o_fsize,fsize=0,temp,_seg *fileoffs,size,lastlen,blockoff;
 char tstr[100]="Saving AUDIOT.",hname[13]="AUDIOHED.";
 char _seg *block;


 strcat(tstr,ext);
 strcat(tstr,"...");
 ErrDialog(tstr,"");

 ////////////////////////////////////////////////
 //
 // AUDIOT.EXT
 // ------------------
 // SAVE PC DATA
 //
 size=MUSEinfo.num_sounds*(sizeof(PCSound)+DATAPOINTS);
 MM_GetPtr((memptr *)&block,size);
 o_fsize=fsize;
 blockoff=0;

 for (i=0;i<MUSEinfo.num_sounds;i++)
   {
    temp=sizeof(PCSound)+PCsound[i].data->common.length;

    MUSEinfo.pcdataoff[i]=fsize;
    MUSEinfo.pcdatalen[i]=temp;
    movedata((unsigned)PCsound[i].data,0,(unsigned)block,blockoff,temp);
    fsize+=temp;
    blockoff+=temp;
    lastlen=fsize;
   }

 SaveFile(projname,(char huge *)block,o_fsize,blockoff);
 MM_FreePtr((memptr *)&block);
 SaveFile(projname,"!ID!",fsize,4);
 fsize+=4;

 //
 // SAVE ADLIB DATA
 //
 size=MUSEinfo.num_sounds*(9+sizeof(AdLibSound)+DATAPOINTS);
 MM_GetPtr((memptr *)&block,size);
 o_fsize=fsize;
 blockoff=0;

 for (i=0;i<MUSEinfo.num_sounds;i++)
   {
    AdLibSound huge *loc;
    temp=9+sizeof(AdLibSound)+AdLibsnd[i].data->common.length;

    MUSEinfo.adlibdataoff[i]=fsize;
    MUSEinfo.adlibdatalen[i]=temp;

    loc=AdLibsnd[i].data;
    movedata(FP_SEG(loc),FP_OFF(loc),FP_SEG(tmpadlib),FP_OFF(tmpadlib),ALBUFSIZE);
    TransAdLibData((AdLibSound huge *)&tmpadlib);

    movedata(FP_SEG(&tmpadlib),FP_OFF(&tmpadlib),(unsigned)block,blockoff,temp-9);
    fsize+=temp-9;
    blockoff+=temp-9;

    // SAVE INST NAME OUT LAST...
    movedata(FP_SEG(&AdLibsnd[i].instname),FP_OFF(&AdLibsnd[i].instname),
	    (unsigned)block,blockoff,9);
    fsize+=9;
    blockoff+=9;
    lastlen=fsize;
   }

 SaveFile(projname,(char huge *)block,o_fsize,blockoff);
 MM_FreePtr((memptr *)&block);

 SaveFile(projname,"!ID!",fsize,4);
 fsize+=4;

 //
 // SAVE DIGITIZED DATA
 //
 for (i=0;i<MUSEinfo.num_sounds;i++)
   {
    MUSEinfo.digidataoff[i]=fsize;
    if (Digisound[i].data)
      {
       temp=sizeof(SampledSound)+Digisound[i].data->common.length;

       MUSEinfo.digidatalen[i]=temp;
       SaveFile(projname,(char huge *)Digisound[i].data,
	 fsize,temp);
       fsize+=temp;
       lastlen=fsize;
      }
   }

 SaveFile(projname,"!ID!",fsize,4);
 fsize+=4;

 //
 // SAVE MUSIC DATA
 //
 for (i=0;i<MUSEinfo.num_music;i++)
 {
  MUSEinfo.musicdataoff[i]=fsize;

  //
  // INDIVIDUALLY LOAD SONGS INTO MEMORY, THEN PURGE (4/28/92)
  //
  if (musicinfo[i].active)
  {
   MUSEinfo.musicdatalen[i]=sizeof(MInfoStr)+musicinfo[i].size;
   MM_GetPtr((memptr *)&block,musicinfo[i].size);
   LoadFile(musicinfo[i].filename,(char huge *)block+2,0,0);
   *(int far *)block = musicinfo[i].size;
//   SaveFile(projname,(char huge *)musicinfo[i].data,fsize,musicinfo[i].size);
   SaveFile(projname,(char huge *)block,fsize,musicinfo[i].size);
   MM_FreePtr((memptr *)&block);

   fsize+=musicinfo[i].size;
  }
  else
    MUSEinfo.musicdatalen[i]=sizeof(MInfoStr);

  SaveFile(projname,(char huge *)&musicinfo[i],fsize,sizeof(MInfoStr));
  fsize+=sizeof(MInfoStr);

  lastlen=fsize;
 }

 ////////////////////////////////////////////////
 //
 // MUSEINFO:
 // -------------------
 // SAVE INFOHEADER OUT
 //
 SaveFile(infoname,(char huge *)&MUSEinfo,0,sizeof(MUSEinfo));
 fsize=sizeof(MUSEinfo);

 //
 // SAVE SOUNDNAMES OUT
 //
 MUSEinfo.soundnamesoff=fsize;
 SaveFile(infoname,(char huge *)&soundnames[0],fsize,MUSEinfo.num_sounds*16);
 fsize+=16*MUSEinfo.num_sounds;

 //
 // SAVE DIGITIZED FILENAMES OUT
 //
 MUSEinfo.digiinfooff=fsize;
 SaveFile(infoname,(char huge *)&digifnames[0],fsize,MUSEinfo.num_sounds*16);
 fsize+=16*MUSEinfo.num_sounds;

 //
 // SAVE INFO OUT AGAIN
 //
 SaveFile(infoname,(char huge *)&MUSEinfo+2,2,sizeof(MUSEinfo)-2);
 SaveHeaders();


 ////////////////////////////////////////////////
 //
 // AUDIOHED.EXT
 // ------------
 // ALLOCATE MEMORY FOR THE "FILEOFFS" ARRAY:
 // "3L*" IS FOR PC/ADLIB/DIGI AND "4L*" IS FOR LONGTYPE
 //
 size=4L*(3L*MUSEinfo.num_sounds+MUSEinfo.num_music+1);
 MM_GetPtr((memptr *)&fileoffs,size);

 for (i=0;i<MUSEinfo.num_sounds;i++)
   {
    fileoffs[i]=MUSEinfo.pcdataoff[i];
    fileoffs[i+MUSEinfo.num_sounds]=MUSEinfo.adlibdataoff[i];
    fileoffs[i+2*MUSEinfo.num_sounds]=MUSEinfo.digidataoff[i];
   }

 for (i=0;i<MUSEinfo.num_music;i++)
   fileoffs[i+3*MUSEinfo.num_sounds]=MUSEinfo.musicdataoff[i];

 fileoffs[MUSEinfo.num_sounds*3+MUSEinfo.num_music]=lastlen;

 strcat(hname,ext);
 SaveFile(hname,(char huge *)fileoffs,0,size);
 MM_FreePtr((memptr *)&fileoffs);


 RestoreBackground();
}



/////////////////////////////////////////////////
//
// Load the AUDIO.ext file in
//
/////////////////////////////////////////////////
void LoadAudio(void)
{
 word i,ow,ot;


 ow=whichsound;
 ot=whichtype;

 LoadFile(infoname,(char huge *)&MUSEinfo,0,sizeof(MUSEinfo));
 LoadFile(infoname,(char huge *)&soundnames,MUSEinfo.soundnamesoff,
   MUSEinfo.num_sounds*16);

 for (i=0;i<MUSEinfo.num_sounds;i++)
 {
  /////////////////////////
  //
  // LOAD THE PC SOUNDS IN
  //
  MM_GetPtr((memptr *)&PCsound[i].data,PCBUFSIZE);
  _fmemset(PCsound[i].data,0,PCBUFSIZE);
  LoadFile(projname,(char huge *)PCsound[i].data,
    MUSEinfo.pcdataoff[i],MUSEinfo.pcdatalen[i]);

  whichsound=i;
  whichtype=PC;
  if (PCsound[i].data->common.length>DATAPOINTS)
    FindDataLength();

  /////////////////////////
  //
  // LOAD THE ADLIB SOUNDS IN
  //
  MM_GetPtr((memptr *)&AdLibsnd[i].data,ALBUFSIZE);
  _fmemset(AdLibsnd[i].data,0,ALBUFSIZE);

  #if 0
  // LOAD INSTNAME IN
  LoadFile(projname,(char huge *)&AdLibsnd[i].instname,
    MUSEinfo.adlibdataoff[i],9);
  LoadFile(projname,(char huge *)AdLibsnd[i].data,
    MUSEinfo.adlibdataoff[i]+9,MUSEinfo.adlibdatalen[i]-9);
  #endif

  #if 1
  LoadFile(projname,(char huge *)AdLibsnd[i].data,
    MUSEinfo.adlibdataoff[i],MUSEinfo.adlibdatalen[i]-9);

  whichtype=ADLIB;
  if (AdLibsnd[i].data->common.length>DATAPOINTS)
    FindDataLength();

  // LOAD INSTNAME IN
  LoadFile(projname,(char huge *)&AdLibsnd[i].instname,
    MUSEinfo.adlibdataoff[i]+MUSEinfo.adlibdatalen[i]-9,9);
  #endif
  RevTransAdLibData(AdLibsnd[i].data);

  #if 0
  /////////////////////////
  //
  // LOAD THE DIGITIZED SOUNDS IN
  //
  MM_GetPtr((memptr *)&Digisound[i].data,MUSEinfo.digidatalen[i]);
  LoadFile(projname,(char huge *)Digisound[i].data,
    MUSEinfo.digidataoff[i],MUSEinfo.digidatalen[i]);
  #endif
 }

 /////////////////////////
 //
 // LOAD MUSIC IN
 //
 for (i=0;i<MUSEinfo.num_music;i++)
 {
  LoadFile(projname,(char huge *)&musicinfo[i],
	   MUSEinfo.musicdataoff[i]+MUSEinfo.musicdatalen[i]-sizeof(MInfoStr),
	   sizeof(MInfoStr));
  //
  // JUST LOAD INFO IN, NOT ACTUAL SONG (4/28/92)
  //
  #if 0
  MM_GetPtr((memptr *)&musicinfo[i].data,musicinfo[i].size);
  LoadFile(projname,(char huge *)musicinfo[i].data,
	   MUSEinfo.musicdataoff[i],musicinfo[i].size);
  #endif
 }

 whichsound=ow;
 whichtype=ot;
}


/////////////////////////////////////////////////
//
// Create the .H and .EQU header files
//
/////////////////////////////////////////////////
void SaveHeaders(void)
{
 #define TAB	3

 FILE *fp;
 word i,j,thetabs,num;
 char fname[14];
 time_t t;


 if (!writeH)
   return;

 time(&t);
 strcpy(fname,"AUDIO");
 strcat(fname,ext);
 strcat(fname,".H");
 unlink(fname);
 if ((fp=fopen(fname,"wt"))==NULL)
   return;

 fprintf(fp,"/////////////////////////////////////////////////\n");
 fprintf(fp,"//\n");
 fprintf(fp,"// MUSE Header for .%s\n",ext);
 fprintf(fp,"// Created %s",ctime(&t));
 fprintf(fp,"//\n");
 fprintf(fp,"/////////////////////////////////////////////////\n");

 fprintf(fp,"\n");

 fprintf(fp,"#define NUMSOUNDS\x9\x9%d\n",MUSEinfo.num_sounds);
 fprintf(fp,"#define NUMSNDCHUNKS\x9\x9%d\n\n",MUSEinfo.num_sounds*3+MUSEinfo.num_music);

 if (MUSEinfo.num_sounds)
 {
  fprintf(fp,"//\n");
  fprintf(fp,"// Sound names & indexes\n");
  fprintf(fp,"//\n");

  fprintf(fp,"typedef enum {\n");
  for (num=i=0;i<MUSEinfo.num_sounds;i++)
    if (PCsound[i].data)
      {
       char name[60];

       strcpy(name,strupr(soundnames[i]));
       strcat(name,"SND");
       for (j=0;j<strlen(name);j++)
	 if (name[j]==' ')
	   name[j]='_';

       strcat(name,",");
       while(strlen(name)<25)
	 strcat(name," ");

       fprintf(fp,"\t\t%s// %d\n",name,i);
       num++;
      }
  fprintf(fp,"\t\tLASTSOUND\n\t     } soundnames;\n");

  fprintf(fp,"\n");
 }

 fprintf(fp,"//\n");
 fprintf(fp,"// Base offsets\n");
 fprintf(fp,"//\n");

 fprintf(fp,"#define STARTPCSOUNDS		0\n");
 fprintf(fp,"#define STARTADLIBSOUNDS	%d\n",MUSEinfo.num_sounds);
 fprintf(fp,"#define STARTDIGISOUNDS		%d\n",MUSEinfo.num_sounds*2);
 fprintf(fp,"#define STARTMUSIC		%d\n",MUSEinfo.num_sounds*3);

 fprintf(fp,"\n");

 if (MUSEinfo.num_music)
 {
  fprintf(fp,"//\n");
  fprintf(fp,"// Music names & indexes\n");
  fprintf(fp,"//\n");

  fprintf(fp,"typedef enum {\n");

  for (num=i=0;i<MUSEinfo.num_music;i++)
    {
     char tname[60];

     strcpy(tname,musicinfo[i].name);
     strcat(tname,"_MUS,");
     while(strlen(tname)<25)
       strcat(tname," ");
     fprintf(fp,"\t\t%s// %d\n",tname,i);
     num++;
    }

  fprintf(fp,"\t\tLASTMUSIC\n\t     } musicnames;\n");
 }

 fprintf(fp,"\n");

 fprintf(fp,"/////////////////////////////////////////////////\n");
 fprintf(fp,"//\n");
 fprintf(fp,"// Thanks for playing with MUSE!\n");
 fprintf(fp,"//\n");
 fprintf(fp,"/////////////////////////////////////////////////\n");

 fclose(fp);
}


/////////////////////////////////////////////////
//
// Return the address of the current sound's data
//
/////////////////////////////////////////////////
char huge *GetDataAddr(void)
{
 char huge *Data;

 switch(whichtype)
 {
  case PC:
    Data=((char huge *)PCsound[whichsound].data->data);
    break;
  case ADLIB:
    Data=((char huge *)AdLibsnd[whichsound].data->data);
 }

 return Data;
}


/////////////////////////////////////////////////
//
// Return the length of the current sound's data
//
/////////////////////////////////////////////////
word GetDataLen(void)
{
 word ret;

 switch(whichtype)
 {
  case PC:
    ret=PCsound[whichsound].data->common.length;
    break;
  case ADLIB:
    ret=AdLibsnd[whichsound].data->common.length;
 }

 return ret;
}


/////////////////////////////////////////////////
//
// Set the length of the current sound's data
//
/////////////////////////////////////////////////
void SetDataLen(word len)
{
 switch(whichtype)
 {
  case PC:
    PCsound[whichsound].data->common.length=len;
    break;
  case ADLIB:
    AdLibsnd[whichsound].data->common.length=len;
 }
}



/////////////////////////////////////////////////
//
// Transpose & scale AdLib data
//
/////////////////////////////////////////////////
void TransAdLibData(AdLibSound huge *loc)
{
 word i,temp0;

 for (i=0;i<DATAPOINTS;i++)
   if (loc->data[i])
     loc->data[i]=adtrans2table[loc->data[i]];
}


/////////////////////////////////////////////////
//
// Reverse-transpose & un-scale AdLib data
//
/////////////////////////////////////////////////
void RevTransAdLibData(AdLibSound huge *loc)
{
 word i,temp0;


 for (i=0;i<DATAPOINTS;i++)
   if (loc->data[i])
     loc->data[i]=adtranstable[loc->data[i]];
}


/////////////////////////////////////////////////
//
// Draw the end line
//
/////////////////////////////////////////////////
void DrawEndLine(void)
{
 word curlen;

 curlen=GetDataLen();
 MouseHide();
 if (curlen+WINDOW_LEFT+1<=WINDOW_RIGHT)
   VW_Vlin(WINDOW_TOP,WINDOW_BOTTOM,curlen+WINDOW_LEFT,ENDCOLOR);
 MouseShow();
}


/////////////////////////////////////////////////
//
// Find length of data
//
/////////////////////////////////////////////////
void FindDataLength(void)
{
 int i;

 switch(whichtype)
 {
  case PC:
     for(i=DATAPOINTS-1;i>=0;i--)
       if (PCsound[whichsound].data->data[i]>=WINDOW_TOP)
	 {
	  PCsound[whichsound].data->common.length=i+1;
	  break;
	 }
     break;
  case ADLIB:
     for(i=DATAPOINTS-1;i>=0;i--)
       if (AdLibsnd[whichsound].data->data[i]>=WINDOW_TOP)
	 {
	  AdLibsnd[whichsound].data->common.length=i+1;
	  break;
	 }
 }
}


////////////////////////////////////////////////////
//
// Create an OBJ linkable file from any type of datafile
//
// Exit:
//  0 = everything's a-ok!
// -1 = file not found
// -2 = file >64K
//
////////////////////////////////////////////////////
int MakeOBJ(char *filename,char *destfilename,char *public,segtype whichseg,char *farname)
{
 char THEADR[17]={0x80,14,0,12,32,32,32,32,32,32,32,32,32,32,32,32,0},
      COMENT[18]={0x88,0,0,0,0,'M','a','k','e','O','B','J',' ','v','1','.','1',0},
      LNAMES[42]={0x96,0,0,
		  6,'D','G','R','O','U','P',
		  5,'_','D','A','T','A',
		  4,'D','A','T','A',
		  0,
		  5,'_','T','E','X','T',
		  4,'C','O','D','E',
		  8,'F','A','R','_','D','A','T','A'},
      SEGDEF[9]={0x98,7,0,0x48,0,0,2,3,4},	// for .DATA
      SEGDEF1[9]={0x98,7,0,0x48,0,0,5,6,4},	// for .CODE
      SEGDEF2[9]={0x98,7,0,0x60,0,0,8,7,4},	// for .FARDATA
      GRPDEF[7]={0x9a,4,0,1,0xff,1,0x61},
      MODEND[5]={0x8a,2,0,0,0x74};

 unsigned i,j,flag,handle;
 long fsize,offset,loffset,temp,amtleft,amount,offset1;
 char _seg *dblock,*block;


 //
 // Need to compute the CHECKSUM in the COMENT field
 // (so if the "MakeOBJ..." string is modified, the CHECKSUM
 //  will be correct).
 //
 COMENT[1]=sizeof(COMENT)-3;
 for (flag=i=0;i<sizeof(COMENT);i++)
    flag+=COMENT[i];
 COMENT[sizeof(COMENT)-1]=(flag^0xff)+1;

 if ((handle=open(filename,O_BINARY))==NULL)
   return -1;

 fsize=filelength(handle);
 close(handle);
 if (fsize>0x10000L)		// BIGGER THAN 1 SEG = ERROR!
   return -2;

 LoadIn(filename,(memptr *)&block);	// LOAD FILE IN
 offset=0;

 MM_GetPtr((memptr *)&dblock,0x10000L);

 ////////////////////////////////////////////////////
 //
 // INSERT HEADER RECORD
 //
 movedata(_DS,FP_OFF(&THEADR),(unsigned)dblock,offset,sizeof(THEADR));
 movedata(FP_SEG(filename),FP_OFF(filename),
	  (unsigned)dblock,offset+4,strlen(filename));
 offset+=sizeof(THEADR);


 ////////////////////////////////////////////////////
 //
 // INSERT COMMENT RECORD
 //
 movedata(_DS,FP_OFF(COMENT),(unsigned)dblock,offset,sizeof(COMENT));
 offset+=sizeof(COMENT);


 ////////////////////////////////////////////////////
 //
 // INSERT START OF LIST-OF-NAMES RECORD
 //
 loffset=offset;
 movedata(_DS,FP_OFF(LNAMES),(unsigned)dblock,offset,sizeof(LNAMES));
 offset+=sizeof(LNAMES);

 // If it's a .FARDATA segment, we need to insert the segment name!
 if (whichseg==FARDATA)
   {
    *(dblock+offset)=strlen(farname);
    movedata(FP_SEG(farname),FP_OFF(farname),
	(unsigned)dblock,offset+1,strlen(farname));
    offset+=strlen(farname)+1;
   }

 // Now, finish the List-Of-Names record by creating
 // the CHECKSUM and LENGTH
 temp=offset;
 offset=offset-loffset-2;
 *(int huge *)(dblock+loffset+1)=offset;
 offset=temp;

 // Now, figure out the CHECKSUM of the record
 for (flag=i=0;i<(offset-loffset);i++)
   flag+=*(dblock+i+loffset);
 *(dblock+offset)=(flag^0xff)+1;
 offset++;


 ////////////////////////////////////////////////////
 //
 // CREATE SEGMENT DEFINITION RECORD
 //
 loffset=offset;
 temp=fsize;
 switch(whichseg)
 {
  case DATA:
    movedata(FP_SEG(&SEGDEF),FP_OFF(&SEGDEF),
	     (unsigned)dblock,offset,sizeof(SEGDEF));
    *(int huge *)(dblock+offset+4)=temp;
    offset+=sizeof(SEGDEF);
    break;
  case CODE:
    movedata(FP_SEG(&SEGDEF1),FP_OFF(&SEGDEF1),
	     (unsigned)dblock,offset,sizeof(SEGDEF1));
    *(int huge *)(dblock+offset+4)=temp;
    offset+=sizeof(SEGDEF1);
    break;
  case FARDATA:
    movedata(FP_SEG(&SEGDEF2),FP_OFF(&SEGDEF2),
	     (unsigned)dblock,offset,sizeof(SEGDEF2));
    *(int huge *)(dblock+offset+4)=temp;
    offset+=sizeof(SEGDEF2);
    break;
 }

 // CHECKSUM
 for (flag=0,i=loffset;i<offset;i++)
   flag+=*(dblock+i);
 *(dblock+offset)=(flag^0xff)+1;
 offset++;


 ////////////////////////////////////////////////////
 //
 // CREATE GROUP DEFINITION RECORD
 //
 switch(whichseg)
 {
  case DATA:
  case CODE:
    movedata(FP_SEG(&GRPDEF),FP_OFF(&GRPDEF),
	     (unsigned)dblock,offset,sizeof(GRPDEF));
    offset+=sizeof(GRPDEF);
 }


 ////////////////////////////////////////////////////
 //
 // CREATE PUBLIC DEFINITION RECORD
 //
 loffset=offset;
 *(dblock+offset)=0x90;		// PUBDEF ID
 offset+=3;			// point to public base, skip length
 *(dblock+offset)=1;		// group index=1
 *(dblock+offset+1)=1;		// segment index=1
 offset+=2;			// point to public name

 temp=0;
 movedata(FP_SEG(public),FP_OFF(public),
	  (unsigned)dblock,offset+1,strlen(public));
 *(dblock+offset)=strlen(public);
 offset+=strlen(public)+1;
 *(int huge *)(dblock+offset)=0;	// public offset within segment
 offset+=2;
 *(dblock+offset)=0;		// type index
 offset++;

 // LENGTH
 temp=offset-loffset-2;
 *(int huge *)(dblock+loffset+1)=temp;
 offset++;

 // CHECKSUM
 for (flag=0,i=loffset;i<offset;i++)
   flag+=*(dblock+i);
 *(dblock+offset)=(flag^0xff)+1;


 ////////////////////////////////////////////////////
 //
 // DATA RECORD(S). YUCK.
 //

 amtleft=fsize;
 amount=1024;
 for (i=0;i<(fsize+1023)/1024;i++)
   {
    offset1=offset;
    if (amtleft<1024)
      amount=amtleft;
    //
    // RECORD HEADER
    //
    *(dblock+offset)=0xa0;			// LEDATA ID
    *(int huge *)(dblock+offset+1)=amount+4;	// length of record
    offset+=3;
    *(dblock+offset)=1;				// segment index
    *(int huge *)(dblock+offset+1)=i*1024;	// index into segment
    offset+=3;
    //
    // LOAD DATA IN
    //
    LoadFile(filename,(char huge *)dblock+offset,i*1024,amount);
    offset+=amount;
    //
    // CHECKSUM!
    //
    for (flag=0,j=offset1;j<offset;j++)
      flag+=*(dblock+j);
    *(dblock+offset)=(flag^0xff)+1;
    offset++;

    amtleft-=1024;
   }

 ////////////////////////////////////////////////////
 //
 // MODULE END! YES!
 //
 movedata(FP_SEG(&MODEND),FP_OFF(&MODEND),(unsigned)dblock,offset,sizeof(MODEND));
 offset+=sizeof(MODEND);

 //
 // Save the little puppy out!
 //
 SaveFile(destfilename,(char huge *)dblock,0,offset);
 MM_FreePtr((memptr *)&dblock);
 MM_FreePtr((memptr *)&block);
 return 0;
}


/////////////////////////////////////////////////
//
// Save current sound in the UNDO buffer
//
/////////////////////////////////////////////////
void SaveUNDO(void)
{
 char huge *Data;


 memset(UNDObuf,0,DATAPOINTS);
 Data=GetDataAddr();
 _fmemcpy((char far *)UNDObuf,(char far *)Data,GetDataLen());
 while(keydown[0x16]);
}


/////////////////////////////////////////////////
//
// Check to see if AUDIO files need to be saved
//
/////////////////////////////////////////////////
void CheckToSave(void)
{
 if (DirtyFlag)
   switch(Message("The AUDIO file has been\nmodified! Save it?"))
   {
    case 0: return;
    case 2: SaveAudio();
	    DirtyFlag=0;
   }
}


/////////////////////////////////////////////////
//
// Find an instrument in the bank file
// Return -1 for Not Found, else return index #
//
/////////////////////////////////////////////////
int FindInstrument(char *name)
{
 instn huge *inames;
 long instdata;
 int numins,i;


 numins = *((word huge *)(InstBank + 8));
 inames = (instn huge *)(InstBank + *((long huge *)(InstBank + 12)));

 strupr(name);
 for (i=0;i<numins;i++)
   if (!_fstrcmp(_fstrupr(inames[i].name),(char far *)name))
     return i;

 return -1;
}


/////////////////////////////////////////////////
//
// Find an instrument that matches the closest to "name"
// Return -1 for Not Found, else return index #
//
/////////////////////////////////////////////////
int SearchInstrument(char *name)
{
 instn huge *inames;
 long instdata;
 int numins,i;


 numins = *((word huge *)(InstBank + 8));
 inames = (instn huge *)(InstBank + *((long huge *)(InstBank + 12)));

 strupr(name);
 for (i=0;i<numins;i++)
   if (!_fstrncmp(_fstrupr(inames[i].name),(char far *)name,strlen(name)))
     return i;

 return -1;
}


// JAB
void CompileInstrument2(unsigned char *insbuf,char huge *dest)
{
struct 	blerk {
			byte	mode,
					voice;

			byte	mksl,
					mmulti,
					mfeedback,
					mattack,
					msustain,
					meg,
					mdecay,
					mrelease,
					mlevel,
					mtremolo,
					mvibrato,
					mksr,
					mconnect;

			byte	cksl,
					cmulti,
					cfeedback,
					cattack,
					csustain,
					ceg,
					cdecay,
					crelease,
					clevel,
					ctremolo,
					cvibrato,
					cksr,
					cunused;

			byte	mwave,
					cwave;
		} far *i = (struct blerk *)insbuf;
 Instrument inst;
#if 1

	inst.mChar =
	(
		(i->mtremolo? 0x80 : 0)
	|	(i->mvibrato? 0x40 : 0)
	|	(i->meg? 0x20 : 0)
	|	(i->mksr? 0x10 : 0)
	|	(i->mmulti & 0x0f)
	);
	inst.mScale =
	(
		(i->mksl << 6)
	|	(i->mlevel & 0x3f)
	);
	inst.mAttack =
	(
		(i->mattack << 4)
	|	(i->mdecay & 0x0f)
	);
	inst.mSus =
	(
		(i->msustain << 4)
	|	(i->mrelease & 0x0f)
	);
	inst.mWave = (i->mwave & 3);

	inst.cChar =
	(
		(i->ctremolo? 0x80 : 0)
	|	(i->cvibrato? 0x40 : 0)
	|	(i->ceg? 0x20 : 0)
	|	(i->cksr? 0x10 : 0)
	|	(i->cmulti & 0x0f)
	);
	inst.cScale =
	(
		(i->cksl << 6)
	|	(i->clevel & 0x3f)
	);
	inst.cAttack =
	(
		(i->cattack << 4)
	|	(i->cdecay & 0x0f)
	);
	inst.cSus =
	(
		(i->csustain << 4)
	|	(i->crelease & 0x0f)
	);
	inst.cWave = (i->cwave & 3);

	inst.nConn =
	(
		(i->cfeedback << 1)
	||	(i->mconnect? 0 : 1)	// DEBUG - really invert?
	);
#else
 unsigned char mchar,cchar,mscale,cscale,mattack,cattack,
	   msus,csus,mwave,cwave,nconn;

 //
 // THIS CHUNK OF BULLSHIT CODE TAKES THE 30 BYTES OF
 // INSTRUMENT BANK DATA AND MUNGES IT INTO THE BYTES
 // THAT GET STORED DIRECTLY INTO THE ADLIB'S REGISTERS!
 //
 mchar=((insbuf[11]&1)<<7)|	// AM
	   ((insbuf[12]&1)<<6)|	// VIB
	   ((insbuf[7]&1)<<5) |	// EG
	   ((insbuf[13]&1)<<4)|	// KSR
	   (insbuf[3]&15);		// MULTI
 cchar=((insbuf[24]&1)<<7)|	// AM
	   ((insbuf[25]&1)<<6)|	// VIB
	   ((insbuf[20]&1)<<5)|	// EG
	   ((insbuf[26]&1)<<4)|	// KSR
	   (insbuf[16]&15);		// MULTI

 mscale=((insbuf[2]&3)<<6)|	// KSL
	(insbuf[10]&63);	// TL
 cscale=((insbuf[15]&3)<<6)|	// KSL
	(insbuf[23]&63);	// TL

 mattack=((insbuf[5]&15)<<4)|	// AR
	 (insbuf[8]&15);	// DR
 cattack=((insbuf[18]&15)<<4)|	// AR
	 (insbuf[21]&15);	// DR

 msus=((insbuf[6]&15)<<4)|	// SL
	   (insbuf[9]&15);		// RR
 csus=((insbuf[19]&15)<<4)|	// SL
	   (insbuf[22]&15);		// RR

 mwave=(insbuf[28]&3);		// WS
 cwave=(insbuf[29]&3);		// WS

 nconn=((insbuf[17]&7)<<1)|	// FB was 4
	(insbuf[14]&1);		// C

#if 0	// DEBUG!!!
 if (insbuf[0]==1 && insbuf[1]!=6)
   {
	cchar=mchar;
	cscale=mscale;
	cattack=mattack;
	csus=msus;
	cwave=mwave;
	mchar=mscale=mattack=msus=mwave=0;
   }
#endif

 inst.mChar=mchar;
 inst.cChar=cchar;
 inst.mScale=mscale;
 inst.cScale=cscale;
 inst.mAttack=mattack;
 inst.cAttack=cattack;
 inst.mSus=msus;
 inst.cSus=csus;
 inst.mWave=mwave;
 inst.cWave=cwave;
 inst.nConn=nconn;
 inst.voice=insbuf[1];
 inst.mode=insbuf[0];
#endif
 movedata(FP_SEG(&inst),FP_OFF(&inst),FP_SEG(dest),FP_OFF(dest),sizeof(Instrument));
}

/////////////////////////////////////////////////
//
// Compile a standard AdLib instrument from the
// Instrument Bank into a special Id 16-byte format
//
/////////////////////////////////////////////////
void CompileInstrument(int instnum,char huge *dest)
{
 instn huge *inames;
 long lastoff,instdata;
 unsigned char insbuf[30];


 inames = (instn huge *)(InstBank + *((long huge *)(InstBank + 12)));
 instdata= *((long huge *)(InstBank+16));
 lastoff=instdata+30L*inames[instnum].doffset;
 movedata((unsigned)InstBank,lastoff,
	  FP_SEG(&insbuf),FP_OFF(&insbuf),sizeof(insbuf));
 CompileInstrument2(insbuf,dest);
}
